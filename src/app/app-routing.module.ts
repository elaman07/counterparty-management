import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { CounterpartyCardComponent } from "./modules/counterparty-card/counterparty-card.component";
import {
    ContractsOfTheCounterpartyComponent
} from "./modules/contracts-of-the-counterparty/contracts-of-the-counterparty.component";

const routes: Routes = [
    {
        path: '',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                redirectTo: '/card-counterparty',
                pathMatch: 'full'
            },
            {
                path: 'card-counterparty',
                component: CounterpartyCardComponent
            },
            {
                path: 'contracts-counterparty',
                component: ContractsOfTheCounterpartyComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
