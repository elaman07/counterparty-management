import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractsOfTheCounterpartyComponent } from './contracts-of-the-counterparty.component';

describe('ContractsOfTheCounterpartyComponent', () => {
  let component: ContractsOfTheCounterpartyComponent;
  let fixture: ComponentFixture<ContractsOfTheCounterpartyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ContractsOfTheCounterpartyComponent]
    });
    fixture = TestBed.createComponent(ContractsOfTheCounterpartyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
