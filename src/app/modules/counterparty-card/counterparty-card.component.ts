import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { CounterpartyService } from "../../services/counterparty.service";
import { MatTableDataSource } from "@angular/material/table";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { DecimalPipe } from "@angular/common";
import { MatExpansionPanel } from "@angular/material/expansion";
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CounterpartyData, CounterpartyModel, GetCounterpartiesResponse } from "../../models/counterparty.model";
import { DateAdapter } from "@angular/material/core";
import { MatDialog } from "@angular/material/dialog";
import {
  CounterpartyCreateEditFormComponent
} from "../counterparty-create-edit-form/counterparty-create-edit-form.component";
import { ACTION, CONFIG, MESSAGE } from "../../shared/const/const";
import { MatSnackBar } from "@angular/material/snack-bar";
import { DeleteConfirmationComponent } from "../../shared/delete-confirmation/delete-confirmation.component";

@Component({
  selector: 'app-counterparty-card',
  templateUrl: './counterparty-card.component.html',
  styleUrls: ['./counterparty-card.component.scss']
})
export class CounterpartyCardComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild('filterPanel') filterPanel!: MatExpansionPanel;

  users!: CounterpartyModel[];
  counterpartiesList!: CounterpartyModel[];
  statusMappings: { [key: string]: string } = {
    'ACTIVE': 'Активен',
    'NOT_ACTIVE': 'Заблокирован',
  };

  rolMappings: { [key: string]: string } = {
    'true': 'Администратор',
    'false': 'Пользователь',
  };

  filterForm!: FormGroup;
  selectedItems = 0;
  selectedCounterparty: CounterpartyModel | null;
  displayedColumns: string[] = ['position', 'actions', 'login', 'email', 'phone', 'rol', 'changedDate', 'creationDate', 'status', 'cash'];
  dataSource = new MatTableDataSource<CounterpartyModel>(this.users);
  decimalPipe = new DecimalPipe(navigator.language);
  filterPanelOpened = false;
  selectAllChecked: boolean = false;
  checkInvalidDate: boolean = false;
  length = 100;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;

  constructor(
    private counterpartyService: CounterpartyService,
    private dateAdapter: DateAdapter<any>,
    private dialog: MatDialog,
    private message: MatSnackBar,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.fillForm();
    this.getCounterParties();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.matTableConfiguration();
  }

  fillForm() {
    this.filterForm = this.formBuilder.group({
      login: [''],
      phone: [''],
      creationDate: ['',],
      status: [''],
      email: ['', [Validators.email]],
      rol: [''],
      changedDate: [''],
    });
  }

  isValidDate(control: AbstractControl): any {
    const inputValue = control.value;
    if (inputValue === '') {
      this.checkInvalidDate = false;
      return null;
    } else {
      this.checkInvalidDate = true;
    }
    const inputDate = this.dateAdapter.parse(inputValue, 'DD.MM.YYYY');
    if (inputDate === null || isNaN(inputDate.getTime())) {
      this.checkInvalidDate = true;
      return {invalidDate: true};
    } else {
      this.checkInvalidDate = false;
    }
    return this.dateAdapter.isValid(inputDate) ? null : {invalidDate: true};
  }


  getCounterParties() {
    this.counterpartyService.fetchData();
    this.counterpartyService.getUsers().subscribe((data: GetCounterpartiesResponse) => {
      this.users = data.users;
      this.users?.forEach((item: CounterpartyModel) => {
        const getStatus: CounterpartyData | undefined = data.data.find((el: CounterpartyData) => el.user_id === item.id);
        if (getStatus) {
          item.is_admin = getStatus.is_admin;
          item.is_ecp = getStatus.is_ecp;
          item.status = getStatus.status;
        }
        item.color = item.status === 'ACTIVE';
        item.created_at = new Date(item.create_at);
        item.updated_at = new Date(item.update_at);
      })
      if (this.users) {
        this.counterpartiesList = [...this.users];
      }
      this.users = this.counterpartyService.getStoredData();
      this.dataSource = new MatTableDataSource<CounterpartyModel>(this.users);
    });
  }

  updateStatuses(status: string) {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent,
      {
        width: '500px',
        data: {
          message:
            status === 'unlock'
              ? this.selectedItems > 1
                ? MESSAGE.UNBLOCK_COUNTERPARTIES
                : MESSAGE.UNBLOCK_COUNTERPARTY
              : this.selectedItems > 1
                ? MESSAGE.BLOCK_COUNTERPARTIES
                : MESSAGE.BLOCK_COUNTERPARTY
        }

      }
    );

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.users = this.users.map((el: CounterpartyModel) => {
          if (this.selectedItems === this.users.length || this.selectedCounterparty?.id === el.id) {
            el.checked = false;
            el.updated_at = new Date();
            el.color = status === 'unlock';
            this.selectAllChecked = false;
            el.status = status === 'unlock' ? 'ACTIVE' : 'NOT_ACTIVE';
          }
          return el;
        });
        if (this.selectedItems > 1) {
          this.message.open(MESSAGE.COUNTERPARTIES_UPDATED_SUCCESSFULLY, ACTION, CONFIG)
        } else {
          this.message.open(MESSAGE.COUNTERPARTY_UPDATED_SUCCESSFULLY, ACTION, CONFIG)
        }
        this.counterpartyService.edit(this.users);
        this.selectedItems = 0;
        this.dataSource = new MatTableDataSource<CounterpartyModel>(this.users);
      }
    })
  }

  matTableConfiguration() {
    this.paginator._intl.itemsPerPageLabel = "Отображать";
    this.paginator._intl.nextPageLabel = "Следующая страница";
    this.paginator._intl.lastPageLabel = "Последняя страница";
    this.paginator._intl.previousPageLabel = "Предыдущая страница";
    this.paginator._intl.firstPageLabel = "Первая страница";
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      const start = page * pageSize + 1;
      const end = (page + 1) * pageSize;
      return `записей ${start} - ${end} из ${this.decimalPipe.transform(length)}`;
    };
  }

  paginatorListener(event: PageEvent) {
    this.length = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    const startIndex = this.pageIndex * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    this.dataSource.data = this.users.slice(startIndex, endIndex);
  }


  toggleAllCheckboxes() {
    this.users.forEach((counterparty: CounterpartyModel) => {
      counterparty.checked = this.selectAllChecked;
      this.selectedItems = this.selectAllChecked ? this.users.length : 0
    })
  }

  toggleCheckboxes(counterparty: CounterpartyModel) {
    this.selectedCounterparty = counterparty.checked ? counterparty : null;
    counterparty.checked ? this.selectedItems++ : this.selectedItems--;
    this.selectAllChecked = this.selectedItems === this.users.length;
  }

  toggleFilterPanel() {
    this.filterPanelOpened = !this.filterPanelOpened;
    this.filterPanel.toggle();
  }

  clearInput(formControlName: string): void {
    this.filterForm.controls[formControlName].setValue('');
  }

  isFormValid(): boolean {
    const isDirty = this.filterForm.dirty;
    const isEmailValid = !this.filterForm.hasError('email', 'email');

    return isDirty && isEmailValid;
  }

  getButtonClass(): string {
    const isDirty = this.filterForm.dirty;
    const isEmailValid = !this.filterForm.hasError('email', 'email');
    if (!isDirty || !isEmailValid) {
      return 'filter-button-style';
    } else {
      return '';
    }
  }

  openCreateEditModal(data?: CounterpartyModel) {
    const dialogRef = this.dialog.open(CounterpartyCreateEditFormComponent, {
      width: '600px',
      data: data
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getCounterParties();
      }
    });
  }

  deleteCounterparty(counterparty: CounterpartyModel): void {
    const dialogRef = this.dialog.open(DeleteConfirmationComponent,
      {
        width: '500px',
        data: {
          message: MESSAGE.DELETE_COUNTERPARTY
        }
      }
    );

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.users = this.users.filter(user => user.id !== counterparty.id);
        this.counterpartyService.edit(this.users);
        this.selectedItems = 0;
        this.dataSource = new MatTableDataSource<CounterpartyModel>(this.users);
        this.message.open(MESSAGE.COUNTERPARTY_DELETED_SUCCESSFULLY, ACTION, CONFIG)
      }
    })
  }


  getButtonColors(): string {
    if (!this.selectedItems) {
      return 'filter-button-style';
    } else {
      return '';
    }
  }


  resetAll() {
    this.filterForm.patchValue({
      login: '',
      phone: '',
      creationDate: '',
      status: '',
      email: '',
      rol: '',
      changedDate: '',
    })
    this.getCounterParties();
  }

  disableResetButton() {
    this.filterForm.markAsPristine();
  }

  applyFilter() {
    this.users = this.filterData(this.counterpartiesList);
    this.dataSource = new MatTableDataSource<CounterpartyModel>(this.users);
  }

  filterData(data: CounterpartyModel[]) {
    return data.filter((item: CounterpartyModel) => {
      const login = this.filterForm.value.login;
      const phone = this.filterForm.value.phone;
      const creationDate = this.filterForm.value.creationDate;
      const status = this.filterForm.value.status;
      const email = this.filterForm.value.email;
      const rol = this.filterForm.value.rol;
      const changedDate = this.filterForm.value.changedDate;
      return (
        (login === '' || item.name.toLowerCase().includes(login.toLowerCase())) &&
        (phone === '' || item.phone.toString().includes(phone.toString())) &&
        (!creationDate || creationDate.getTime() === item.create_at) &&
        (status === '' || status === item.status) &&
        (email === '' || item.email.toLowerCase().includes(email.toLowerCase())) &&
        (rol === '' || item.is_admin === rol) &&
        (!changedDate || changedDate.getTime() === item.update_at)
      );
    });
  }

}
