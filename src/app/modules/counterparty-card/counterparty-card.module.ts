import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CounterpartyCardComponent } from "./counterparty-card.component";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatCardModule } from "@angular/material/card";
import { MatButtonModule } from "@angular/material/button";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule, MatOptionModule } from "@angular/material/core";
import { MatSelectModule } from "@angular/material/select";
import { HttpClientModule } from "@angular/common/http";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatTableModule } from "@angular/material/table";
import { MatDividerModule } from "@angular/material/divider";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { NgxMaskDirective, NgxMaskPipe, provideNgxMask } from 'ngx-mask';
import { MatDialogModule } from "@angular/material/dialog";
import { MatTooltipModule } from "@angular/material/tooltip";



@NgModule({
  declarations: [CounterpartyCardComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatNativeDateModule,
    MatButtonModule,
    FormsModule,
    MatDatepickerModule,
    MatOptionModule,
    HttpClientModule,
    MatSelectModule,
    NgxMaskDirective,
    NgxMaskPipe,
    MatDialogModule,
    MatPaginatorModule,
    MatTableModule,
    MatDividerModule,
    MatExpansionModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatTooltipModule
  ],
  providers: [provideNgxMask()]
})
export class CounterpartyCardModule { }
