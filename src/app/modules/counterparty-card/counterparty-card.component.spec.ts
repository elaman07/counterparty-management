import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterpartyCardComponent } from './counterparty-card.component';

describe('CounterpartyCardComponent', () => {
  let component: CounterpartyCardComponent;
  let fixture: ComponentFixture<CounterpartyCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CounterpartyCardComponent]
    });
    fixture = TestBed.createComponent(CounterpartyCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
