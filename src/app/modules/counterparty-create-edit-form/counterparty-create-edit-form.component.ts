import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { CounterpartyService } from "../../services/counterparty.service";
import { CounterpartyModel } from "../../models/counterparty.model";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ACTION, CONFIG, MESSAGE } from "../../shared/const/const";

@Component({
  selector: 'app-counterparty-create-edit-form',
  templateUrl: './counterparty-create-edit-form.component.html',
  styleUrls: ['./counterparty-create-edit-form.component.scss']
})
export class CounterpartyCreateEditFormComponent implements OnInit{
  form!: FormGroup;
  counterpartyList: CounterpartyModel[];
  counterparty: CounterpartyModel;

  constructor(
    private formBuilder: FormBuilder,
    private counterpartyService: CounterpartyService,
    private message: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: CounterpartyModel,
    private dialogRef: MatDialogRef<CounterpartyCreateEditFormComponent>
  ) {}

  ngOnInit() {
    this.createForm();
    this.fillForm();
  }

  createForm() {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      create_at: [''],
      id: [''],
      status: ['', [Validators.required]],
      email: ['', [Validators.email]],
      is_admin: ['', [Validators.required]],
      update_at: [''],
    });
  }

fillForm() {
   this.counterpartyList = this.counterpartyService.getStoredData();
   if (this.data?.id) {
     const data = this.counterpartyList.find(item => item.id === this.data.id);
     if (data) {
       this.form.patchValue(data);
     }
   }
}


  clearInput(formControlName: string): void {
    this.form.controls[formControlName].setValue('');
  }

  getButtonClass(): string {
    const isDirty = this.form.valid;
    const isEmailValid = !this.form.hasError('email', 'email');
    if (!isDirty || !isEmailValid) {
      return 'filter-button-style';
    } else {
      return '';
    }
  }

  create() {
    if (this.data && this.data.id) {
      this.edit();
    } else {
      const randomID = Math.floor(Math.random() * 1000000);
      this.form.value.create_at = Math.floor(new Date().getTime() / 1000);
      this.form.value.update_at = Math.floor(new Date().getTime() / 1000);
      this.form.value.created_at = new Date().toLocaleString('en-US', { timeZone: 'UTC' });
      this.form.value.updated_at = new Date().toLocaleString('en-US', { timeZone: 'UTC' });
      this.form.value.id = randomID;
      this.form.value.color = this.form.value.status === 'ACTIVE';
      this.counterparty = this.form.value;
      this.counterpartyService.create(this.counterparty);
      this.dialogRef.close(true);
      this.message.open(MESSAGE.COUNTERPARTY_CREATED_SUCCESSFULLY, ACTION, CONFIG)
    }
  }

  edit() {
    const update_at = Math.floor(new Date().getTime() / 1000);
    const updated_at = new Date().toLocaleString('en-US', { timeZone: 'UTC' });
    const color = this.form.value.status === 'ACTIVE';
    const edit = this.counterpartyList.find(item => item.id === this.data.id);
    if (edit) {
      Object.assign(edit, {
        name: this.form.value.name,
        email: this.form.value.email,
        color,
        update_at,
        updated_at,
        status: this.form.value.status,
        is_admin: this.form.value.is_admin,
        phone: this.form.value.phone,
      });
    }
    this.counterpartyService.edit(this.counterpartyList);
    this.dialogRef.close(true);
    this.message.open(MESSAGE.COUNTERPARTY_UPDATED_SUCCESSFULLY, ACTION, CONFIG)
  }


  closeModal() {
    this.dialogRef.close();
  }

  resetAll() {
    this.form.patchValue({
      login: '',
      phone: '',
      creationDate: '',
      status: '',
      email: '',
      rol: '',
      changedDate: '',
    })
  }

  disableResetButton() {
    this.form.markAsPristine();
  }

}
