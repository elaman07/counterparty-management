import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterpartyCreateEditFormComponent } from './counterparty-create-edit-form.component';

describe('CounterpartyCreateEditFormComponent', () => {
  let component: CounterpartyCreateEditFormComponent;
  let fixture: ComponentFixture<CounterpartyCreateEditFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CounterpartyCreateEditFormComponent]
    });
    fixture = TestBed.createComponent(CounterpartyCreateEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
