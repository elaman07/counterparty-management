import { MatSnackBarConfig } from "@angular/material/snack-bar";

export const MESSAGE = {
  COUNTERPARTY_UPDATED_SUCCESSFULLY: 'Контрагент успешно обновлен',
  COUNTERPARTIES_UPDATED_SUCCESSFULLY: 'Контрагенты успешно обновлены',
  COUNTERPARTY_CREATED_SUCCESSFULLY: 'Контрагент успешно создан',
  COUNTERPARTY_DELETED_SUCCESSFULLY: 'Контрагент успешно удален',
  DELETE_COUNTERPARTY: 'Вы уверены, что хотите удалить этого контрагента?',
  BLOCK_COUNTERPARTY: 'Вы уверены, что хотите заблокировать этого контрагента?',
  UNBLOCK_COUNTERPARTY: 'Вы уверены, что хотите разблокировать этого контрагента?',
  BLOCK_COUNTERPARTIES: 'Вы уверены, что хотите заблокировать выбранных контрагентов?',
  UNBLOCK_COUNTERPARTIES: 'Вы уверены, что хотите разблокировать выбранных контрагентов?',
}

export const CONFIG:  MatSnackBarConfig = {
  horizontalPosition: 'right',
    verticalPosition: 'top',
}

export const ACTION: string =  'Закрыть'
