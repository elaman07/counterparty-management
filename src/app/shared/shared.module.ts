import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from "./navbar/navbar.component";
import { RouterLink, RouterLinkActive, RouterModule } from "@angular/router";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { MatListModule } from "@angular/material/list";
import { MatSelectModule } from "@angular/material/select";
import { DeleteConfirmationComponent } from "./delete-confirmation/delete-confirmation.component";
import { MatDialogModule } from "@angular/material/dialog";



@NgModule({
  declarations: [NavbarComponent, SidebarComponent, DeleteConfirmationComponent],
  imports: [
    RouterModule,
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    RouterLink,
    RouterLinkActive,
    MatButtonModule,
    MatListModule,
    MatSelectModule,
    MatDialogModule,
  ],
  exports: [NavbarComponent, SidebarComponent]
})
export class SharedModule { }
