import { AfterViewInit, Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-sidebar', templateUrl: './sidebar.component.html', styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements AfterViewInit {
  isShowSidebar = false;


  ngAfterViewInit(): void {
    this.logScreenWidth();
  }

  @HostListener('window:resize', ['$event']) onResize(event: Event): void {
    setTimeout(() => {
      this.logScreenWidth();
    });
  }

  private logScreenWidth() {
    const screenWidth = window.innerWidth;
    this.isShowSidebar = screenWidth > 1024;
  }

}
