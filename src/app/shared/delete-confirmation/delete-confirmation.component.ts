import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { MESSAGE } from "../const/const";

@Component({
  selector: 'app-delete-confirmation',
  templateUrl: './delete-confirmation.component.html',
  styleUrls: ['./delete-confirmation.component.scss']
})
export class DeleteConfirmationComponent implements OnInit {

  MESSAGE = MESSAGE;
  checkMessage = false;
  checkMessageDelete = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<DeleteConfirmationComponent>) {
  }

  ngOnInit(): void {
    this.checkMessage = this.data.message === MESSAGE.UNBLOCK_COUNTERPARTY || this.data.message === MESSAGE.UNBLOCK_COUNTERPARTIES;
    this.checkMessageDelete = this.data.message === MESSAGE.DELETE_COUNTERPARTY;
  }

  closeDialog() {
    this.dialogRef.close(false);
  }

}
