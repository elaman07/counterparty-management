import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { CounterpartyService } from "../../services/counterparty.service";
import { CounterpartyModel, GetCounterpartiesResponse } from "../../models/counterparty.model";

@Component({
  selector: 'app-navbar', templateUrl: './navbar.component.html', styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  @Input() isMenuOpened!: boolean;
  @Output() isShowSidebar = new EventEmitter<boolean>();
  users: CounterpartyModel[];
  userName: string;
  checkProfile = false;

  constructor(private userService: CounterpartyService) {
    this.userService.getUsers().subscribe((data: GetCounterpartiesResponse) => {
      this.users = data.users;
      this.users?.forEach((item: CounterpartyModel) => {
        this.userName = item.name;
      })
    });
  }

  ngAfterViewInit(): void {
    this.logScreenWidth();
  }

  @HostListener('window:resize', ['$event']) onResize(event: Event): void {
    setTimeout(() => {
      this.logScreenWidth();
    });
  }

  openMenu() {
    this.isMenuOpened = !this.isMenuOpened;
    this.isShowSidebar.emit(this.isMenuOpened);
  }

  private logScreenWidth() {
    const screenWidth = window.innerWidth;
    if (screenWidth > 1024) {
      this.checkProfile = true;
    } else {
      this.checkProfile = false;
    }
  }
}
