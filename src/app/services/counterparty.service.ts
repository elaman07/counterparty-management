import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { BehaviorSubject } from "rxjs";
import { CounterpartyModel, GetCounterpartiesResponse } from "../models/counterparty.model";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CounterpartyService {
  counterpartyList: CounterpartyModel[];

  private apiUrl = environment.apiUrl + '/rubetek/angular-testcase-list/';
  private dataSubject: BehaviorSubject<GetCounterpartiesResponse> = new BehaviorSubject<GetCounterpartiesResponse>({} as GetCounterpartiesResponse);
  private data$: Observable<GetCounterpartiesResponse> = this.dataSubject.asObservable();

  constructor(private http: HttpClient) {
  }

  fetchData() {
    if (!this.dataSubject.value || !Object.keys(this.dataSubject.value).length) {
      this.http.get<GetCounterpartiesResponse>(this.apiUrl).subscribe((data) => {
        this.dataSubject.next(data);
        data.users.forEach(item => {
          if (!this.counterpartyList.some(existingItem => existingItem.id === item.id)) {
            this.counterpartyList.push(item);
          }
        });
        localStorage.setItem('counterpartyList', JSON.stringify(this.counterpartyList));
      });
    }
  }

  getUsers(): Observable<GetCounterpartiesResponse> {
    return this.data$;
  }

  create(data: CounterpartyModel): void {
    this.counterpartyList.push(data);
    localStorage.setItem('counterpartyList', JSON.stringify(this.counterpartyList));
  }

  edit(data: CounterpartyModel[]): void {
    this.counterpartyList = data;
    localStorage.setItem('counterpartyList', JSON.stringify(this.counterpartyList));
  }

  getStoredData(): CounterpartyModel[] {
    const storedData = localStorage.getItem('counterpartyList');
    this.counterpartyList = storedData ? JSON.parse(storedData) : [];
    return this.counterpartyList;
  }
}
