import { AfterViewInit, Component, HostListener, ViewChild } from '@angular/core';
import { MatSidenav } from "@angular/material/sidenav";

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements AfterViewInit{
  @ViewChild('sidenav') sidenav!: MatSidenav;
  isShowSidebar: boolean = true;

  constructor() {}

  ngAfterViewInit(): void {
    Promise.resolve().then(() => this.logScreenWidth())
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: Event): void {
    setTimeout(() => {
      this.logScreenWidth();
    });
  }

  private logScreenWidth() {
    const screenWidth = window.innerWidth;
    if (screenWidth > 1024) {
      this.isShowSidebar = true;
      this.sidenav.open();
    } else {
      this.isShowSidebar = false;
      this.sidenav.close();
    }
  }
}

