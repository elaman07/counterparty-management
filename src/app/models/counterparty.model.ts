export interface CounterpartyModel {
  name: string;
  email: string;
  position: number;
  phone: number;
  checked: boolean;
  id: number;
  color: boolean;
  is_admin: boolean;
  create_at: string;
  created_at: Date;
  update_at: string;
  updated_at: Date;
  status: CounterpartyStatusType;
  is_ecp: boolean;
}

export interface CounterpartyData {
  is_admin: boolean;
  is_ecp: boolean;
  status: CounterpartyStatusType;
  user_id: number;
}

export type CounterpartyStatusType = 'ACTIVE' | 'NOT_ACTIVE';
export interface GetCounterpartiesResponse {
  data: CounterpartyData[];
  page: PageModel;
  users: CounterpartyModel[];
}

export interface PageModel {
  current: number;
  size: number;
  total: number;
}
